import spotipy
import sys
import random as rndm
from spotipy.oauth2 import SpotifyClientCredentials


client_credentials_manager = SpotifyClientCredentials(
    client_id="", client_secret="")
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

if len(sys.argv) > 1:
    name = ' '.join(sys.argv[1:])
else:
    name = 'Ecco2k'

# search for artist
results = sp.search(q='artist:' + name, type='artist')
items = results['artists']['items']
print(results)
if len(items) > 0:
    # take first result
    artist = items[0]
    artist_uri = "spotify:artist:" + artist['id']
    tracks_result = sp.artist_top_tracks(artist_uri)
    track = rndm.choice(tracks_result['tracks'][:10])
    print(track['name'])
