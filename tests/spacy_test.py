import spacy
from spacy import displacy
from collections import Counter

nlp = spacy.load("en_core_web_sm")

doc = nlp('I like Ecco2k and ASAPRocky')
print(doc)
print([(X.text, X.label_) for X in doc.ents])
print(doc.ents[0])
