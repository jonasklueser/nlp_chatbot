import random
import json
import pickle
import numpy as np

import nltk
from nltk.stem import WordNetLemmatizer

import tensorflow as tf

import spacy
from spacy import displacy
from collections import Counter

import spotipy
import random as rndm
from spotipy.oauth2 import SpotifyClientCredentials

# conect to spotify API with Spotipy library
client_credentials_manager = SpotifyClientCredentials(
    client_id="", client_secret="")
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

lemmatizer = WordNetLemmatizer()
intents = json.loads(open('intents.json'). read())

words = pickle.load(open('words.pkl', 'rb'))
classes = pickle.load(open('classes.pkl', 'rb'))
model = load_model('chatbot_model.h5')

# clean up sentences, tokenize and lemmatize similar words


def clean_up_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [lemmatizer.lemmatize(word) for word in sentence_words]
    return sentence_words

# get entities out of sentence


def entity_reaction(sentence, context):
    nlp = spacy.load("en_core_web_sm")
    entity_in_sentence = nlp(sentence)
    # print(entity_in_sentence.ents)
    if (context == 'music_answer'):
        # first Word recognized as Person is processed
        # print(entity_in_sentence)
        name = entity_in_sentence.ents[0]
        name = str(name)

        # search for artist in spotify API
        results = sp.search(q='artist:' + name, type='artist')
        items = results['artists']['items']
        # print(results)
        if len(items) > 0:
            # take first result
            artist = items[0]
            artist_uri = "spotify:artist:" + artist['id']
            # print(artist)
            tracks_result = sp.artist_top_tracks(artist_uri)
            track = random.choice(tracks_result['tracks'][:10])
            # print(track['name'])
            return(track['name'])
        return("none")


# create bag of words


def bag_of_words(sentence):
    sentence_words = clean_up_sentence(sentence)
    bag = [0] * len(words)
    for w in sentence_words:
        for i, word in enumerate(words):
            if word == w:
                bag[i] = 1

    return np.array(bag)

# predicts class of the sentence using both functions above


def predict_class(sentence):
    bow = bag_of_words(sentence)
    res = model.predict(np.array([bow]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i, r] for i, r in enumerate(res) if r > ERROR_THRESHOLD]

    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({'intent': classes[r[0]], 'probability': str(r[1])})
    return return_list


def get_response(intents_list, intents_json, sentence):
    tag = intents_list[0]['intent']
    list_of_intents = intents_json['intents']
    for i in list_of_intents:
        if i['tag'] == tag:
            if tag == 'music_answer':
                result = random.choice(i['responses']) + \
                    entity_reaction(sentence, tag)
            else:
                result = random.choice(i['responses'])
            break
    return result


print("Chatbot just woke up!")

while True:
    message = input("")
    ints = predict_class(message)
    res = get_response(ints, intents, message)
    print(res)
